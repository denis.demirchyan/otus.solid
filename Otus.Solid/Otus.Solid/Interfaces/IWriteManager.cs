namespace Otus.Solid.Interfaces
{
    public interface IWriteManager
    {
        void Write(string text);
    }
}