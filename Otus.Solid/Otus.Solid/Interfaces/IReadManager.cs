namespace Otus.Solid.Interfaces
{
    public interface IReadManager
    {
        string Read();
    }
}