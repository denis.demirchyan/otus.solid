namespace Otus.Solid.Interfaces
{
    public interface INumberGenerator
    {
        int Generate();
        int Generate(int max);
        int Generate(int min, int max);
    }
}