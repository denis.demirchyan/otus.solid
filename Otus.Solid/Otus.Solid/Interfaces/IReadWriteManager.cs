namespace Otus.Solid.Interfaces
{
    public interface IReadWriteManager : IReadManager, IWriteManager
    {
        
    }
}