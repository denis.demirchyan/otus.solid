using System;
using Otus.Solid.Interfaces;

namespace Otus.Solid
{
    public class ConsoleReadManager : IReadManager
    {
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}