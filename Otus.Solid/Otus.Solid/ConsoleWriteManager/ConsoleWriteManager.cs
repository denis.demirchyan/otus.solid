using System;
using Otus.Solid.Interfaces;

namespace Otus.Solid
{
    public class ConsoleWriteManager : IWriteManager
    {
        public void Write(string text)
        {
            Console.WriteLine(text);
        }
    }
}