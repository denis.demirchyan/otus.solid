using System;
using Otus.Solid.Interfaces;

namespace Otus.Solid
{
    public class ExtNumberGenerator : NumberGenerator, INumberGenerator
    {
        
        public int Generate(int max)
        {
            return Generate(int.MinValue, max);
        }

        public int Generate(int min, int max)
        {
            Random random = new();
            return random.Next(min, max);
        }
    }
}