using System;
using Otus.Solid.Interfaces;

namespace Otus.Solid
{
    public class NumberGenerator
    {
        public virtual int Generate()
        {
            Random random = new();
            return random.Next();
        }
    }
}