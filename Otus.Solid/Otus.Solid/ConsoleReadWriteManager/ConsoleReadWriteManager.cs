using System;
using Otus.Solid.Interfaces;

namespace Otus.Solid
{
    public class ConsoleReadWriteManager : IReadWriteManager
    {
        public void Write(string text)
        {
            Console.WriteLine(text);
        }
        
        public string Read()
        {
            return Console.ReadLine();
        }
    }
}