using Otus.Solid.Interfaces;

namespace Otus.Solid
{
    public class GuessNumberApp
    {
        private readonly IReadWriteManager _readWriteManager;
        private readonly INumberGenerator _numberGenerator;
        private readonly int _minValue;
        private readonly int _maxValue;
        private readonly int _maxAttempts;
        private int _targetNumber;
        
        public GuessNumberApp(IReadWriteManager readWriteManager,
            INumberGenerator numberGenerator,
            int minValue = 0,
            int maxValue = 100,
            int maxAttempts = 5)
        {
            _readWriteManager = readWriteManager;
            _numberGenerator = numberGenerator;
            _minValue = minValue;
            _maxValue = maxValue;
            _maxAttempts = maxAttempts;
        }

        public void Launch()
        {
            _readWriteManager.Write("Game start.");
            GenerateNumber();

            for (var i = _maxAttempts; i > 0; --i)
            {
                _readWriteManager.Write($"Attempts: {i}.");
                var userNumber = GetUserNumber();

                var isWin = CheckNumber(userNumber, out var message);  
                _readWriteManager.Write(message);

                if (isWin) return;
            }
            
            _readWriteManager.Write($"You lose. Target value was {_targetNumber}.");
        }

        private bool CheckNumber(int userNumber, out string message)
        {
            if (_targetNumber == userNumber)
            {
                message = "You win!";
                return true;
            }
            
            message = _targetNumber > userNumber ? "Target value is greater;" : "Target value is less;";
            return false;
        }

        private int GetUserNumber()
        {
            _readWriteManager.Write($"Input your number.");
            return int.Parse(_readWriteManager.Read());
        }
        private void GenerateNumber()
        {
            _readWriteManager.Write($"Generate number from {_minValue} to {_maxValue}.");
            _targetNumber = _numberGenerator.Generate(_minValue, _maxValue);
            _readWriteManager.Write("Number generated");
        }
    }
}