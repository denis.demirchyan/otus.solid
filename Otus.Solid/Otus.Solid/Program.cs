﻿using System;
using System.Configuration;

namespace Otus.Solid
{
    class Program
    {
        static void Main(string[] args)
        {
            var minValue = int.Parse(ConfigurationManager.AppSettings.Get("minValue")!);
            var maxValue = int.Parse(ConfigurationManager.AppSettings.Get("maxValue")!);
            var maxAttempts = int.Parse(ConfigurationManager.AppSettings.Get("maxAttempts")!);

            ConsoleReadWriteManager readWriteManager = new();
            ExtNumberGenerator numberGenerator = new();

            var app = new GuessNumberApp(readWriteManager, numberGenerator, minValue, maxValue, maxAttempts);
            app.Launch();
        }
    }
}
